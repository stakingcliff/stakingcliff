StakingCliff and SCF just started

https://stakingcliff.net

SCF is designed for safe profitable investment in cryptocurrencies. 

The main goal of the StakingCliff is to create and implement a stable, promising economy.

The proceeds from token sales are used to fill the StakingCliff validators. The validators are located on the StakingCliff servers and are maintained by experienced technicians and programmers to ensure reliable and trouble-free operation. Initially, they are included in the list of projects: Cosmos, Polkadot, Near Protocol, Cello, NuCypher and CasperLabs.

Network support fees received by the StakingCliff validators support the SCF course and are distributed through a smart contract to token holders. 

It is planned to create three investment deposit models: for three months, six months and one year. 

The project plans to obtain the status of super representative in the Tron network. Those who voted for StakingCliff will be awarded with SCF tokens.

StakingCliff is created for the community and provides a discussion platform on the discord server and in the telegram chat, a news channel on the website and in telegrams. 

https://tronscan.org/#/token/1003446

Telegram news and chat

https://t.me/stakingcliff

discord server

https://discord.gg/2SyFmvk


